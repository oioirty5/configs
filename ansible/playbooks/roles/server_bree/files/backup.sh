#!/usr/bin/env bash
set -euo pipefail

todayDate=$(date --iso-8601)

mysqldump matomo > /root/matomo_${todayDate}.sql
mysqldump bookstack > /root/bookstack_${todayDate}.sql

# Disable
systemctl stop smokeping

# Murmur
# Dokuwiki # TODO remove
# Hastebin data
# Smokeping
# Matomo
# Bookstack
tar -acvf /root/bree-${todayDate}.tzst \
/var/db/murmur/murmur.sqlite \
/usr/share/webapps/dokuwiki \
/etc/webapps/dokuwiki \
/var/lib/dokuwiki \
/usr/lib/node_modules/haste/data \
/srv/smokeping/data \
/root/matomo_${todayDate}.sql \
/root/bookstack_${todayDate}.sql

# Upload to my Nextcloud instance
cloudsend --file /root/bree-${todayDate}.tzst

# Cleanup
rm /root/matomo_${todayDate}.sql
rm /root/bookstack_${todayDate}.sql
rm /root/bree-${todayDate}.tzst

# Enable
systemctl start smokeping
