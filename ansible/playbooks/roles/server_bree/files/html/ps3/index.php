<?php
function formatBytes(float $size, $precision = 2) {
	$base = log($size, 1024);
	$suffixes = array('', 'K', 'M', 'G', 'T');
	return round(pow(1024, $base - floor($base)), $precision) .''. $suffixes[floor($base)];
}
function About(){
	echo("Licensed under AGPLv3 - <a href=https://gitlab.com/C0rn3j/configs/tree/master/sc1/ps3>https://gitlab.com/C0rn3j/configs/tree/master/sc1/ps3</a><br>
By Martin Rys - <a href=https://rys.pw>https://rys.pw</a><br>
Inspired by <a href=http://www.eurasia.nu/psn_update_finder.php>http://www.eurasia.nu/psn_update_finder.php</a>");
}

// Create context options that disable certificate verification because SONY can"t be arsed to keep their TLS certs up to date
$arrContextOptions=array(
	"ssl"=>array(
			"verify_peer"=>false,
			"verify_peer_name"=>false,
	),
);
if($_GET["titleid"]) {
	$titleID=$_GET["titleid"];
	$rawxml = file_get_contents("https://a0.ww.np.dl.playstation.net/tpl/np/$titleID/$titleID-ver.xml", false, stream_context_create($arrContextOptions));
} else {
	$rawxml = file_get_contents("https://a0.ww.np.dl.playstation.net/tpl/np/BCES01118/BCES01118-ver.xml", false, stream_context_create($arrContextOptions)); // Resistance (4)
}
//$rawxml = file_get_contents("https://a0.ww.np.dl.playstation.net/tpl/np/BCES00647/BCES00647-ver.xml", false, stream_context_create($arrContextOptions)); // Buzz (1)
//$rawxml = file_get_contents("https://a0.ww.np.dl.playstation.net/tpl/np/BCES01118/BCES01118-ver.xml", false, stream_context_create($arrContextOptions)); // Resistance (4)
// np (Retail), prod-qa (Debugish), sp-int (Debug), q-np (Unknown)

echo("<!DOCTYPE html>
<html>
	<head>
		<title>PS3 Update Checker</title>
		<style>
		table, th, td {
			border: 1px solid black;
		}
		th, td { padding: 4px; }
		</style>
	</head>
<body>");

// https://www.psdevwiki.com/ps3/Online_Connections#Game_Updating_Procedure
if (! $xml=simplexml_load_string($rawxml)) {
	echo("Error - No result for <b>".$_GET['titleid']."</b><br><br>");
	About();
	exit(1);
}
//print_r($xml);
//echo($xml->asXML());
// Always 'alive'?
//echo("Status: ".$xml->attributes()->status."<br>\n");
// TitleID + _something
//echo("TagName: ".$xml->tag->attributes()->name."<br>\n");
// May be related to whether update pops up when booting game
//echo("TagPopup: ".$xml->tag->attributes()->popup."<br>\n");
// May be related to being logged to psn or not
//echo("TagSignoff: ".$xml->tag->attributes()->signoff."<br>\n");
$gameTitle="";
echo("<table>
	<thead>
		<tr>
			<th>Version</th>
			<th>Size</th>
			<th>PS3 Min Version</th>
			<th>Checksum (SHA1)</th>
		</tr>
	</thead>
	<tbody>");
foreach ($xml->tag->package as $package) {
//	echo("<br>\n");
	echo("<tr>");
	// Version + Download link
	echo("<td style=\"text-align:center;\"><a href=".$package->attributes()->url."><b>".$package->attributes()->version."</b></a></td>");
	// File size
	echo("<td style=\"text-align:center;\">".formatBytes(floatval($package->attributes()->size))."</td>");
	// Minimum PS3 version
	echo("<td style=\"text-align:center;\">".$package->attributes()->ps3_system_ver."</td>");
	// SHA1
	echo("<td style=\"text-align:center;\">".$package->attributes()->sha1sum."</td>");
	// Could add showing localized version of the name but meh
	// https://www.psdevwiki.com/ps3/PARAM.SFO#TITLE_xx_.28for_localized_languages.29
	echo("</tr>");
	$gameTitle=$package->paramsfo->TITLE;
}
echo("	</tbody>
</table>\n");
echo("<br>\n");
echo("<b>Game:</b> ".$gameTitle."<br>\n");
echo("<b>TitleID:</b> ".$xml->attributes()->titleid."<br>\n");
echo("<br>\n");
// https://a0.ww.np.dl.playstation.net/tpl/np/{productId}/{productId}-ver.xml
// Buzz - 1 package: https://a0.ww.np.dl.playstation.net/tpl/np/BCES00647/BCES00647-ver.xml
// Resistance 3 - 4 packages: https://a0.ww.np.dl.playstation.net/tpl/np/BCES01118/BCES01118-ver.xml
echo("To check the checksum, you have to cut the last 32 bytes from the PKG file as that is the checksum. On Linux - 'head -c -32 file.pkg | sha1sum'<br>\n");
echo("(Someone pointed out there can be packages where the end is also padded by zeroes, if that's the case the above command needs to be improved)<br>\n");
echo("<br>");
About();
echo("</body>
</html>
");
?>
