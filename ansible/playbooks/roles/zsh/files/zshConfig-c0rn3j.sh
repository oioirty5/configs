export EDITOR=nano
export VISUAL=nano
export GOPATH=/home/${USERNAME}/Golang
export GRC_ALIASES=true # otherwise /etc/profile.d/grc.sh refuses to execute

# source /etc/profile.d/grc.sh - this should already be done for login shells, but somehow doesn't seem to apply in KDE and lxc exec(probably because it's not a login shell?)
# We do this first as it otherwise overrides our following aliases
# Note: For ip we use grc instead of 'ip -c', as the -c flag is not as featureful: https://github.com/garabik/grc/issues/180
source /etc/profile.d/grc.sh

# ls, but with colors!
alias ls="lsd"
# Bat is a prettier cat with syntax highlighting and more
alias cat="bat"
# Pydf is a df replacement that has usage bars and colors
alias df="pydf"
# Drop caches and sync
alias clearcaches="sudo sync && echo 3 | sudo tee /proc/sys/vm/drop_caches"
# Set default g++ flags to be strict
alias g++="colourify g++ -pedantic -Wall -Wextra -Wcast-align -Wcast-qual -Wctor-dtor-privacy -Wdisabled-optimization -Wformat=2 -Winit-self -Wlogical-op -Wmissing-declarations -Wmissing-include-dirs -Wnoexcept -Wold-style-cast -Woverloaded-virtual -Wredundant-decls -Wshadow -Wsign-conversion -Wsign-promo -Wstrict-null-sentinel -Wstrict-overflow=5 -Wswitch-default -Wundef -Werror -Wno-unused -Og"
# More verbose lsblk
alias lsblk='colourify lsblk -o +fstype,label,uuid'
alias dmesg='sudo dmesg'
alias blkid='sudo grc -se blkid'
alias corn-info='sudo corn-info'
alias makePKGBUILD="makepkg --printsrcinfo > .SRCINFO && (makepkg -sri || sha512sum ./* || true && sha256sum ./* || true) && makepkg --printsrcinfo > .SRCINFO"
alias dutmpfs='for i in $(mount | grep ^tmpfs | awk {"print \$3"} ); do sudo du -sh ${i} 2>/dev/null; done'

# Execute corn-info on new terminals - sudo is not needed on root but eh
sudo /usr/local/sbin/corn-info

# Autosuggest previously used commands
source /usr/share/zsh/plugins/zsh-autosuggestions/zsh-autosuggestions.zsh
# Syntax highlighting for zsh, line needs to be at the very end of file
source /usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
