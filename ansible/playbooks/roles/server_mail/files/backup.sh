#!/usr/bin/env bash
set -euo pipefail

todayDate=$(date --iso-8601)

mysqldump postfix_db > /root/postfix_db_${todayDate}.sql
mysqldump roundcubemail > /root/roundcubemail_${todayDate}.sql

tar -acvf /root/mail-${todayDate}.tzst \
/root/postfix_db_${todayDate}.sql \
/root/roundcubemail_${todayDate}.sql \
/home/vmail

# Upload to my Nextcloud instance
cloudsend --file /root/mail-${todayDate}.tzst

# Cleanup
rm /root/postfix_db_${todayDate}.sql
rm /root/roundcubemail_${todayDate}.sql
rm /root/mail-${todayDate}.tzst
