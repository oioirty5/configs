#!/usr/bin/env bash
set -euo pipefail

todayDate=$(date --iso-8601)
backupFilename="/root/Guard_Guild-${todayDate}.tzst"

sed "/token:/c\token: \'yourtokenhere\'" /root/the-guard-bot/config.js > /tmp/config.js
tar --exclude='node_modules' --exclude='/root/the-guard-bot/config.js' -acvf ${backupFilename} /root/the-guard-bot /tmp/config.js
echo "Backup of Guard saved as ${backupFilename}"
echo "Uploading backup to C0rn3j..."
cloudsend --file ${backupFilename}
echo "Upload to C0rn3j done!"

rm -f ${backupFilename} /tmp/config.js
