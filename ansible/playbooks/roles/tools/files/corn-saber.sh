#!/usr/bin/env bash
set -euo pipefail
Red='\033[0;31m'
Green='\033[0;32m'
Yellow='\033[0;33m'
NoColor='\033[0m'

installModsQbeat() {
	WINEPREFIX="${HOME}/wine/beatsaber"
	#winetricks dotnet461
	# https://github.com/geefr/beatsaber-linux-goodies/releases
	cd ~/Downloads/QBeatBeta6
	./QBeat --config set winePrefix ${WINEPREFIX}
	./QBeat --config set bsInstall "${HOME}/.steam/steam/steamapps/common/Beat Saber"
	./QBeat --config set gameVersion "1.12.2"
	./QBeat --validate-wine
	./QBeat --install "BSIPA"
	./QBeat --patch

	declare -a mods=(
		"BeatSaverDownloader"
		"BeatSaverVoting"
		"Enhanced Search And Filters" # not yet ported to 1.12.2
		"MappingExtensions"
		"NoFailCheck"
		"PerfectionDisplay" # not yet ported to 1.12.2
		"ScoreSaber"
		"ScorePercentage"
		"SongCore"
		"SongDataCore"
	)

	for mod in "${mods[@]}"; do
		if ! ./QBeat --install "$mod"; then
			echo -e "${Red}Failed downloading mod ${mod}! It probably isn't available for this game version!${NoColor}"
		fi
	done
}

installMods() {
	# https://github.com/Ominitay/ShellSaber
	#./shaber-install
	# Arch bug workaround # TODO report it
	PATH=$PATH:~/.local/bin
	WINEPREFIX="${HOME}/wine/beatsaber"
	# Install BSIPA
	shaber ipa download
	shaber ipa patch

	declare -a mods=(
		"BeatSaverDownloader"
		"BeatSaverVoting"
		"Enhanced Search And Filters" # not yet ported to 1.12.2
		"MappingExtensions"
		"NoFailCheck"
		"PerfectionDisplay" # not yet ported to 1.12.2
		"ScoreSaber"
		"ScorePercentage"
		"SongCore"
		"SongDataCore"
	)

	for mod in "${mods[@]}"; do
#		shaber mod disable "$mod" || true
#		shaber mod remove "$mod" || true
		if ! shaber mod download "$mod"; then
			echo -e "${Red}Failed downloading mod ${mod}! It probably isn't available for this game version!${NoColor}"
		else
			shaber mod enable "$mod"
		fi
	done
}

downloadSongs() {
	cd ~/Downloads/BeatSyncConsole
	chmod +x ./BeatSyncConsole
	./BeatSyncConsole
}

downloadSongs
#installModsQbeat
installMods
