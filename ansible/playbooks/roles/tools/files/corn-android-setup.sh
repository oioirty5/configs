#!/bin/bash
set -euo pipefail
Red='\033[0;31m'
Green='\033[0;32m'
Yellow='\033[0;33m'
NoColor='\033[0m'

Help() {
	echo -e "${Yellow}"
	cat << EOF
This is a personalized script for setup of my Redmi 9T (EEA)

Problems:
* Google Pay doesn't work on stock ROM. Works on LineageOS though.
* Can't figure out a way to do automatic call recording on LineageOS
* Can't do gestures over a keyboard in LineageOS

Prop values were figured out while toggling the values via:
 corn-diff --sleep 1 --command "adb shell su -c 'settings list global'" --change --permanent
 corn-diff --sleep 1 --command "adb shell su -c 'settings list --lineage global'" --change --permanent
 corn-diff --sleep 1 --command "adb shell su -c 'settings list system'" --change --permanent
 corn-diff --sleep 1 --command "adb shell su -c 'settings list --lineage system'" --change --permanent
 corn-diff --sleep 1 --command "adb shell su -c 'settings list secure'" --change --permanent
 corn-diff --sleep 1 --command "adb shell su -c 'settings list --lineage secure'" --change --permanent
Randomass XML file while toggling via:
 corn-diff --sleep 1 --change --permanent --command "adb shell su -c 'find /data -mmin 1'"

Options:
--miui                             Run setup for MIUI(12) stock ROM
--lineage                          Run setup for LineageOS(17.1)
-h | --help                        Show this help...

Usage example: ./corn-android-setup --lineage
EOF
	echo -e "${NoColor}"
}

SetProp() {
	user=${1}
	namespace=${2}
	propName=${3}
	propValue=${4}
	if adb shell su -c "settings put --user ${user} ${namespace} ${propName} ${propValue}"; then
		echo -e "${Green}Set ${propName}=${propValue} for ${user} in ${namespace}${NoColor}"
	else
		echo -e "${Red}Failed setting ${propName}=${propValue} for ${user} in ${namespace}${NoColor}"
		exit 1
	fi
}

Fdroid() {
	# Install F-Droid
	if [[ $(adb shell pm list packages fdroid | wc -c) != '26' ]]; then # Check because installed version can be newer than downloaded version, resulting in a failure.
		wget -O /tmp/FDroid.apk https://f-droid.org/FDroid.apk
		adb install /tmp/FDroid.apk
	fi
#	echo -e "${Yellow}Downloading Cheat Engine...${NoColor}"
#	wget -q --user-agent="Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36" -O /tmp/CheatEngine.apk http://cheatengine.org/download/CheatEngine.apk
#	adb install /tmp/CheatEngine.apk
	# Temporarily install and use F-Droid over adb to install applications
	# This assumes you have 'go' installed
	GOPATH=/tmp/Golang
	go get -u mvdan.cc/fdroidcl
	$GOPATH/bin/fdroidcl update
	$GOPATH/bin/fdroidcl install com.amaze.filemanager                 # File manager
	$GOPATH/bin/fdroidcl install com.artifex.mupdf.viewer.app          # PDF viewer
	$GOPATH/bin/fdroidcl install com.fsck.k9                           # K-9 Mail
	$GOPATH/bin/fdroidcl install com.google.zxing.client.android       # Barcode scanner
	$GOPATH/bin/fdroidcl install com.kunzisoft.keepass.libre           # Password manager
	$GOPATH/bin/fdroidcl install com.nextcloud.client                  # Nextcloud
	$GOPATH/bin/fdroidcl install com.nextcloud.talk2                   # Nextcloud Talk
	$GOPATH/bin/fdroidcl install com.vrem.wifianalyzer                 # Wi-Fi analyzer
	$GOPATH/bin/fdroidcl install com.termux                            # Termux - terminal emulator
	$GOPATH/bin/fdroidcl install com.termux.boot                       # Termux app letting you autostart services on android boot
	$GOPATH/bin/fdroidcl install fr.gouv.etalab.mastodon               # Mastodon client
	$GOPATH/bin/fdroidcl install io.github.muntashirakon.Music         # Music player
	$GOPATH/bin/fdroidcl install net.sourceforge.opencamera            # FOSS Camera
	$GOPATH/bin/fdroidcl install nodomain.freeyourgadget.gadgetbridge  # For my Mi Band 4 fitness tracker
	$GOPATH/bin/fdroidcl install org.adaway                            # Block ads in hosts file - root required
	$GOPATH/bin/fdroidcl install org.kde.kdeconnect_tp                 # KDEConnect - sync and control between PC and android
	$GOPATH/bin/fdroidcl install org.jitsi.meet                        # Jitsi Meet - audio/video chat
	$GOPATH/bin/fdroidcl install org.schabi.newpipe                    # Youtube FOSS client
	$GOPATH/bin/fdroidcl install org.videolan.vlc                      # VLC - video player
	$GOPATH/bin/fdroidcl install ru.henridellal.dialer                 # Dialer app
	$GOPATH/bin/fdroidcl install ru.gelin.android.weather.notification # Weather notifs for my smartwatch
	$GOPATH/bin/fdroidcl install se.lublin.mumla                       # Mumble client
	$GOPATH/bin/fdroidcl install im.vector.app                         # Element - Matrix client
}

MIUI() {
	# MIUI won't let you enable USB install without logging into MI account - to work around this with root:
	adb shell su -c 'setprop persist.security.adbinstall 1'

	# This command reinstalls the apps removed by pm uninstall:
	#adb shell cmd package install-existing <package name>
	# Granted, your phone must still boot into android to do this.
	# To resolve a soft brick you've to do a full format or use Xiaomi's recovery tool.

	# List all packages: pm list packages -f
	# Uninstalling things you don't understand will break stuff.
	adb shell pm list packages > /dev/null # Check that we can actually use pm before disabling exit on error
	set +e
	# Some apps can be uninstalled fully
	adb uninstall com.facebook.katana
	adb uninstall com.netflix.mediaclient
	#adb uninstall com.alibaba.aliexpresshd
	adb uninstall com.mi.global.bbs # Mi Community
	adb uninstall cn.wps.moffice_eng # WPS office
	adb uninstall com.crazy.juicer.xm # Game
	adb uninstall com.bubble.free.bubblestory # Game
	adb uninstall com.logame.eliminateintruder3d # Game
	adb uninstall com.block.puzzle.game.hippo.mi # Game
	adb uninstall com.micredit.in # Mi Credit
	adb uninstall cn.wps.xiaomi.abroad.lite # Mi Doc Viewer (WPS)
	adb uninstall com.mi.health
	adb uninstall com.miui.android.fashiongallery # Changes lockscreen wallpaper randomly.. also spams notifications with "news"
	# Some can't
	adb shell pm uninstall -k --user 0 com.android.browser
	adb shell pm uninstall -k --user 0 com.android.calendar # I use Google Calendar
	adb shell pm uninstall -k --user 0 com.android.contacts # I use Google Contacts and Emerald Dialer (this also removes the stock Phone/Dialer app)
	adb shell pm uninstall -k --user 0 com.google.ar.lens
	adb shell pm uninstall -k --user 0 com.google.android.apps.docs # Drive
	adb shell pm uninstall -k --user 0 com.google.android.apps.tachyon # Duo
	adb shell pm uninstall -k --user 0 com.google.android.apps.photos
	adb shell pm uninstall -k --user 0 com.google.android.marvin.talkback # Android Accessibility Suite
	adb shell pm uninstall -k --user 0 com.google.android.tts
	adb shell pm uninstall -k --user 0 com.google.android.videos
	adb shell pm uninstall -k --user 0 com.google.android.youtube
	adb shell pm uninstall -k --user 0 com.facebook.appmanager
	adb shell pm uninstall -k --user 0 com.facebook.services
	adb shell pm uninstall -k --user 0 com.facebook.system
	adb shell pm uninstall -k --user 0 com.mi.android.globalminusscreen # App Vault
	adb shell pm uninstall -k --user 0 com.mipay.wallet.in
	adb shell pm uninstall -k --user 0 com.miui.analytics
	adb shell pm uninstall -k --user 0 com.miui.bugreport
	adb shell pm uninstall -k --user 0 com.miui.calculator
	adb shell pm uninstall -k --user 0 com.miui.cleanmaster
	adb shell pm uninstall -k --user 0 com.miui.cloudservice
	adb shell pm uninstall -k --user 0 com.miui.cloudbackup
	adb shell pm uninstall -k --user 0 com.miui.compass
	adb shell pm uninstall -k --user 0 com.miui.fm
	adb shell pm uninstall -k --user 0 com.miui.hybrid
	adb shell pm uninstall -k --user 0 com.miui.miservice # Services & Feedback (MIUI 11+)
	adb shell pm uninstall -k --user 0 com.miui.msa.global # MIUI System Ads
	adb shell pm uninstall -k --user 0 com.miui.notes
	adb shell pm uninstall -k --user 0 com.miui.player
	adb shell pm uninstall -k --user 0 com.miui.securityadd
	adb shell pm uninstall -k --user 0 com.miui.videoplayer
	adb shell pm uninstall -k --user 0 com.miui.yellowpage
	adb shell pm uninstall -k --user 0 com.miui.weather2
	adb shell pm uninstall -k --user 0 com.netflix.partner.activation
	adb shell pm uninstall -k --user 0 com.xiaomi.glgm # Games
	adb shell pm uninstall -k --user 0 com.xiaomi.joyose # ??? runs on startup
	adb shell pm uninstall -k --user 0 com.xiaomi.midrop
	adb shell pm uninstall -k --user 0 com.xiaomi.mipicks
	adb shell pm uninstall -k --user 0 com.xiaomi.payment
	adb shell pm uninstall -k --user 0 com.xiaomi.scanner
	#adb shell pm uninstall -k --user 0 com.facemoji.lite.xiaomi # Facemoji - can't be removed via adb - MIUI 12: Settings -> About Phone -> Storage -> Apps and data -> Facemoji -> uninstall
	#adb shell pm uninstall -k --user 0 com.mi.android.globalpersonalassistant # Not installed anymore on MIUI11
	#adb shell pm uninstall -k --user 0 com.xiaomi.discover # System app updater
	#adb shell pm uninstall -k --user 0 com.xiaomi.finddevice # SOFT BRICK - no boot!
	#adb shell pm uninstall -k --user 0 com.miui.securitycenter # SOFT BRICK - no boot!
	#adb shell pm uninstall -k --user 0 com.android.chrome
	#adb shell pm uninstall -k --user 0 com.android.cellbroadcastreceiver
	#adb shell pm uninstall -k --user 0 com.android.globalpersonalassistant
	#adb shell pm uninstall -k --user 0 com.android.stk
	#adb shell pm uninstall -k --user 0 com.google.android.apps.maps
	#adb shell pm uninstall -k --user 0 com.google.android.googlequicksearchbox
	#adb shell pm uninstall -k --user 0 com.google.android.inputmethod.latin (after replace keyboard)
	#adb shell pm uninstall -k --user 0 com.google.android.music # Play Music
	#adb shell pm uninstall -k --user 0 com.mi.webkit.core
	#adb shell pm uninstall -k --user 0 com.miui.gallery
	#adb shell pm uninstall -k --user 0 com.miui.home (Install NOVA launcher before)
	#adb shell pm uninstall -k --user 0 com.miui.screenrecorder
	set -e

	echo -e "${Yellow}"
	cat << 'EOF'
Settings -> About Phone -> Tap on MIUI Version a few times to unlock developer mode
#         -> Connection & Sharing -> Secure Element position: HCE Wallet (Less secure - only set if NFC payments don't work https://www.tomshardware.com/news/host-card-emulation-secure-element,28804.html)
#                                 -> Tap & pay -> Make sure your preferred payment method is selected if you use NFC payments
#                                              -> Use Default: Replace default app when another payment app is open
         -> Display -> Dark Mode: On
                    -> Brightness level -> Automatic brightness: Off
         -> Wallpaper -> Pick one
         -> Sound & vibration -> Silent/DND -> Schedule turn on time: On (Every day, 00:00->08:00, DND)
                              -> Phone ringtone -> Single SIM settings: On
                                                -> Personal SIM: Phone Lines
                                                -> Work SIM: Mad World (Jasmine Thompson)
         -> Always-on Display & Lock Screen -> Always-on Display: On
                                            -> Show items -> Display items: Always
                                            -> Style: Pick one
                                            -> Sleep: 10 minutes
                                            -> Launch Camera: On
                                            -> Lock screen clock format: Centered
                                            -> Lock screen owner info -> Show signature on Lock screen: On (martin@rys.pw C0rn3j)
         -> Passwords & Security -> Fingerprint unlock -> Show fingerprint icon when the screen is off: Off
         -> Notifications -> Lock screen notifications -> Format: Show notifications but hide content
                          -> KDE Connect -> Persistent indicator -> Show notifications: Off
         -> Home Screen -> App Vault: Off
         -> Additional Settings -> Developer options -> Dark Theme: On
                                                     -> USB Debugging: On
                                -> Date and Time -> Time Format: 24-hour
                                -> Full screen display -> System navigation: Gestures
                                -> Languages & input -> Manage keyboards -> Settings (Gboard)-> Theme -> Dark (Blue)
                                                                                             -> Preferences -> Long press for symbols: On
                                                                                                            -> Show emoji switch key: Off
                                                                                                            -> Emoji browsing suggestions: Off
                                                                                                            -> Number row: On
         -> Special Features -> Front camera effects -> Sound effect: Off
         -> Apps -> Manage apps -> KDE Connect, Nextcloud, Termux: Boot, K-9 Mail, Gadgetbridge, Google Chat -> Autostart
                                                                                                             -> Battery Saver: No Restrictions
                                -> Google Pay -> Other permissions -> NFC: Yes
                 -> System app settings -> Call settings -> Call recording -> Call recording notification: Off
                                                                           -> Record calls automatically: On
                                        -> Security -> Security scan -> Scan before installing: Off
                                        -> Camera -> Watermark -> Device watermark: Off
                                                  -> Video encoder: H.265
                                                  -> Shutter sound: Off
                                                  -> Mirror front camera: Off # Last used camera has to be front camera for this setting to be present!
EOF
	echo -e "${NoColor}"
}

LineageOS() {
	echo -e "${Yellow}Setting props...${NoColor}"
	echo -e "${Yellow}Settings -> Display -> Lock screen display -> Lock screen: Show sensitive content only when unlocked${NoColor}"
	SetProp 0 secure lock_screen_allow_private_notifications 0
	echo -e "${Yellow}Settings -> Display -> Lock screen display -> Ambient display -> On: yes${NoColor}"
	SetProp 0 secure doze_enabled 1
	echo -e "${Yellow}Settings -> Display -> Lock screen display -> Ambient display -> Always on: yes${NoColor}"
	SetProp 0 secure doze_always_on 1
	echo -e "${Yellow}Settings -> Display -> Screen timeout: 5 minutes${NoColor}"
	SetProp 0 system screen_off_timeout 300000
	echo -e "${Yellow}Settings -> Display -> Font size: Small${NoColor}"
	SetProp 0 system font_scale 0.85
	echo -e "${Yellow}Settings -> Display -> Display size: Smaller${NoColor}"
	SetProp 0 system display_density_forced 352
	echo -e "${Yellow}Settings -> System -> Date & Time -> Use 24-hour format: On${NoColor}"
	SetProp 0 system time_12_24 24
	echo -e "${Yellow}Settings -> System -> Developer options -> Enable Wi-Fi Verbose Logging: on${NoColor}"
	SetProp 0 global wifi_verbose_logging_enabled 0
	echo -e "${Yellow}Settings -> System -> Status bar -> Show seconds: Yes${NoColor}"
	SetProp 0 secure clock_seconds 1
	echo -e "${Yellow}Settings -> System -> Status bar -> Clock position: Right${NoColor}"
	SetProp 0 '--lineage system' status_bar_clock 0
	echo -e "${Yellow}Settings -> System -> Status bar -> Battery status style: Text${NoColor}"
	SetProp 0 '--lineage system' status_bar_battery_style 2
	echo -e "${Yellow}Settings -> System -> Status bar -> Brightness slider: Show always${NoColor}"
	SetProp 0 '--lineage secure' qs_show_brightness_slider 2
	echo -e "${Yellow}Settings -> System -> Status bar -> Auto brightness: Off${NoColor}"
	SetProp 0 '--lineage secure' qs_show_auto_brightness 2
	echo -e "${Yellow}Settings -> System -> Status bar -> Brightness control: On${NoColor}"
	SetProp 0 '--lineage system' status_bar_brightness_control 1
	echo -e "${Yellow}Settings -> System -> Status bar -> Network traffic monitor -> Display mode: Upload and download${NoColor}"
	SetProp 0 '--lineage secure' network_traffic_mode 3
	echo -e "${Yellow}Settings -> System -> Status bar -> Network traffic monitor -> Auto hide: on${NoColor}"
	SetProp 0 '--lineage secure' network_traffic_autohide 1
	echo -e "${Yellow}Settings -> System -> Status bar -> Network traffic monitor -> Traffic measurement units: MB/s${NoColor}"
	SetProp 0 '--lineage secure' network_traffic_units 3
	echo -e "${Yellow}Settings -> System -> Gestures -> System navigation -> Gesture navigation: yes${NoColor}"
	SetProp 0 secure navigation_mode 2
	echo -e "${Yellow}Settings -> System -> Gestures -> System navigation -> Gesture navigation -> Show navigation hint: off${NoColor}"
	SetProp 0 '--lineage system' navigation_bar_hint 0
	echo ""
	echo -e "${Yellow}Manual setup required for the following:"
	cat << EOF
Settings -> Display -> Lock screen display -> Lock screen message: martin@rys.pw C0rn3j - call +420 [CENSORED] if found
Settings -> Network & Internet -> Wi-Fi -> Settings for currently connected network -> Advanced -> Privacy: Use device MAC
Settings -> Security -> Add another fingerprint
Settings -> System -> Developer options -> adb: on
Settings -> System -> Developer options -> rooted adb: on
Settings -> System -> Front camera effects: Disable `# /data/data/org.lineageos.devicesettings/shared_prefs/org.lineageos.devicesettings_preferences.xml`
Settings -> Sound -> Do Not Disturb -> Schedules -> Create one (Every day, 00:00->08:00) and enable it

EOF
	echo -e "${NoColor}"
}
General() {
	echo -e "${Yellow}"
	cat << 'EOF'
# Do KDE Connect first for shared clipboard
KDE Connect -> Pair and allow perms

F-Droid -> Settings -> Theme: Dark
F-Droid -> Settings -> Repositories -> + -> https://fdroid.cgeo.org/

Gmail -> Settings -> General settings -> Conversation list density: Compact
Gmail -> Settings -> Work email -> Notifications: Off

Clock -> Daily alarm at 8:50

Weather notification -> API Key: f96741ee05d7e6b97b5dd821fffc3d55
Weather notification -> Gadgetbridge: On
Weather notification -> Weather location: [My city]

Gadgetbridge -> Settings -> Connect to Gadgetbridge device when Bluetooth is turned on: On
Gadgetbridge -> Settings -> Reconnect automatically: On
Gadgetbridge -> Settings -> Theme: Dark
Gadgetbridge -> Settings -> ...also when screen is on: On
Gadgetbridge -> Settings -> Auto fetch activity data: On
Gadgetbridge -> Settings -> Minimum time between fetches: 5
Gadgetbridge -> + -> Long press Mi Band 4 -> Auth key: from KeePassXC
Gadgetbridge -> + -> Long press Mi Band 4 -> Activate display upon lift: On
Gadgetbridge -> + -> Long press Mi Band 4 -> Sync calendar events: On
Gadgetbridge -> + -> Short tap Mi Band 4

# Install - https://forum.xda-developers.com/apps/magisk/official-magisk-v7-universal-systemless-t3473445
# If you use Magisk with non-stock recovery, this is how to flash stock OTA updates - https://forum.xda-developers.com/mi-9t/how-to/guide-ota-updates-stock-recovery-t3975027
Magisk -> Settings -> Hide Magisk Manager: Magicorn [Restart Magisk!]
Magisk -> Settings -> MagiskHide: On
Magisk -> Superuser(shield) -> MagiskHide -> Google Play Services, Airbank, Revolut
Magisk -> Modules(puzzle) -> SQLite, MagiskHide Props Config, Riru (Riru - Core), Riru - Clipboard Whitelist -> Install
Magisk -> Modules(puzzle) -> https://github.com/stylemessiah/GPay-SQLite-Fix/releases -> Install
Magisk -> Make sure that SafetyNet Check passes

# KDEConnect's clipboard sharing from Android->PC doesn't work since Android 10, which is why we installed the Riru modules and now we need to whitelist KDEConnect and reboot
adb shell
su
echo 'org.kde.kdeconnect_tp' > /data/misc/clipboard/whitelist.list

# Change props to pass SafetyNet
adb shell
props
------------------------------------------
ro.build.fingerprint:
Lineage 17.1:        Xiaomi/davinci/davinci:10/QKQ1.190825.002/V12.0.2.0.QFJMIXM:user/release-keys
Stock 12.0.2 EEA:    Xiaomi/davinci/davinci:10/QKQ1.190825.002/V12.0.2.0.QFJEUXM:user/release-keys
props(use this one): Xiaomi/davinci_eea/davinci:10/QKQ1.190825.002/V12.0.2.0.QFJEUXM:user/release-keys

ro.build.version.security_patch:
Lineage 17.1:                   2020-10-05
Stock 12.0.2 EEA(use this one): 2020-07-01
------------------------------------------

Delete default clock/weather widget
Long press home screen -> Widgets -> Google -> At a glance

KeePass DX -> Settings -> Advanced unlocking -> Biometric unlocking: Off

K-9 Mail -> Login -> Folder poll frequency: Every minute
K-9 Mail -> Login -> Notify me while mail is being checked: Off

Mumla -> Add my server

#adb shell am start -a android.intent.action.VIEW -d 'market://details?id=com.alibaba.aliexpresshd'
Install and setup the following apps:
  Adaway -> Enable adblocking
  Air Bank [My Air]         https://play.google.com/store/apps/details?id=cz.airbank.android
  Aliexpress:               https://play.google.com/store/apps/details?id=com.alibaba.aliexpresshd
  Benefit Plus:             https://play.google.com/store/apps/details?id=xx.mbp
  c:geo -> Configure geocaching.com account
  DriveDroid:               https://play.google.com/store/apps/details?id=com.softwarebakery.drivedroid
    Set Image folder to ../ISOs/
    Copy ISO images
    Create blank images for Windows and use corn-windows-bootable on them after connecting them (6500MB FAT32)
  Flowkey:                  https://play.google.com/store/apps/details?id=com.flowkey.app
  Game Guardian
  Google Pay:               https://play.google.com/store/apps/details?id=com.google.android.apps.walletnfcrel
    Add loyalty cards
  Google Chat:              https://play.google.com/store/apps/details?id=com.google.android.apps.dynamite
  Google Calendar:          https://play.google.com/store/apps/details?id=com.google.android.calendar
  Google Contacts:          https://play.google.com/store/apps/details?id=com.google.android.contacts
  Google Meet:              https://play.google.com/store/apps/details?id=com.google.android.apps.meetings
  Google Translate:         https://play.google.com/store/apps/details?id=com.google.android.apps.translate
    Settings -> Tap To Translate -> Enable: On
  Help Scout:               https://play.google.com/store/apps/details?id=net.helpscout.android
  KDEConnect -> Plugin Settings -> Notification Sync -> Gmail, Slack, Steam Chat, Telegram: Disable
  Můj vlak:                 https://play.google.com/store/apps/details?id=cz.cd.mujvlak.an
  Nova Launcher (Prime):    https://play.google.com/store/apps/details?id=com.teslacoilsw.launcher https://play.google.com/store/apps/details?id=com.teslacoilsw.launcher.prime
  PIA:                      https://play.google.com/store/apps/details?id=com.privateinternetaccess.android
  Revolut:                  https://play.google.com/store/apps/details?id=com.revolut.revolut
  Slack:                    https://play.google.com/store/apps/details?id=com.Slack
  Smart Pianist:            https://play.google.com/store/apps/details?id=jp.co.yamaha.smartpianist
  Steam Chat:               https://play.google.com/store/apps/details?id=com.valvesoftware.android.steam.friendsui
  Synthesia:                https://play.google.com/store/apps/details?id=com.synthesia.synthesia
    Unlock Synthesia
    Settings -> Songs [Allow permission]
             -> Color Theme: Classic
             -> Music Devices -> Music Output -> Yamaha -> Prevent "local" notes: On
                              -> Music Input -> Yamaha only
  Telegram:                 https://play.google.com/store/apps/details?id=org.telegram.messenger
    Settings -> Chat Settings -> Dark
             -> Notifications and Sounds -> All Accounts: Off
  Nextcloud -> Settings -> Back up contacts -> Automatic backup: On
            -> Auto Upload -> Camera, OpenCamera(both video and pictures!), Telegram(both video and pictures!), Discord, Screenshots
                           -> Set up a custom folder -> Local folder[/MIUI/sound_recorder], Remote folder[/InstantUpload/sound_recorder]
  Termux:
    pkg upgrade -y && pkg install -y git mlocate nano openssh tmux rsync zsh && termux-setup-storage && ssh-keygen -t ed25519 -q -N "" -f /data/data/com.termux/files/home/.ssh/id_ed25519 && cat /data/data/com.termux/files/home/.ssh/id_ed25519.pub
    echo "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIIufCM34eXtzcLG4cR/UrHFcAQf+x9/xOCwG+SVPD0LH c0rn3j@Luxuria" > /data/data/com.termux/files/home/.ssh/authorized_keys
    sh -c "$(curl -fsSL https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"
    git clone https://github.com/zsh-users/zsh-autosuggestions ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-autosuggestions
    nano ~/.zshrc - plugins=(zsh-autosuggestions); DISABLE_UPDATE_PROMPT=true (before source line); mortalscumbag theme
    mkdir -p ~/.termux/boot && printf "#\!/data/data/com.termux/files/usr/bin/sh\nsshd\n" > ~/.termux/boot/start-sshd
    Launch Termux:Boot app once, reboot
    sshd should now be autoexecuted on boot
    Add pubkey to Ansible at home and sw db
    Make sure MAC matches DHCP record
    Sync my music: rsync -av --progress --delete --omit-dir-times --no-perms -e "ssh -p 8022" ~/Nextcloud/HugeFiles/Music/ root@192.168.1.43:/sdcard/Music/
EOF
	echo -e "${NoColor}"
}

while [[ -n ${1-} ]]; do
	case ${1} in
		--miui )
			Fdroid && MIUI && General
			help="no";;
		--lineage )
			Fdroid && LineageOS && General
			help="no";;
		-h | --help )
			Help
			exit 0;;
		* )
			echo -e "${Red}Wrong parameter(s)${NoColor}"
			Help
			exit 1;;
	esac
	shift
done

if [[ -z ${help-} ]]; then
	Help
	exit 1
fi

connectedDevices=$(adb devices -l)
if ! echo "${connectedDevices}" | grep "device " >/dev/null; then
	echo -e "${Red}adb found no device!${NoColor}"
	exit 1
fi
