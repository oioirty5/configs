#!/usr/bin/env bash
set -euo pipefail
Red='\033[0;31m'
Green='\033[0;32m'
Yellow='\033[0;33m'
NoColor='\033[0m'

Help() {
	echo -e "${Yellow}"
	cat << EOF
This is a script to get diff for the same command over a period of time

Options:
--sleep                            Sleep for X time between commands, defaults to 3
--command                          Command to diff
--change                           Repeat command until there's a change
--permanent                        If specified together with --change, repeat the command indefinitely and show diff for every new change
-h | --help                        Show this help...

Usage example: ./corn-diff --sleep 3 --command 'uptime'
EOF
	echo -e "${NoColor}"
}

main() {
	if ! firstCommand=$(${executedCommand}); then
		echo -e "${Red}Command ${executedCommand} failed!${NoColor}"
		exit 1
	fi
	echo -e "${Yellow}Executed first command. Sleeping for ${sleepTime}...${NoColor}"
	if [[ ${change} == "yes" ]]; then
		while :; do
			sleep "${sleepTime}"
			if ! secondCommand=$(${executedCommand}); then
				echo -e "${Red}Command ${executedCommand} failed!${NoColor}"
				exit 1
			fi
			if diff <(echo "${firstCommand}") <(echo "${secondCommand}"); then
				echo -e "${Yellow}Executed command again with no diff but --change was specified. Sleeping for ${sleepTime} and repeating...${NoColor}"
			else
				if [[ ${permanent} == "yes" ]]; then
					firstCommand=${secondCommand}
				else
					break
				fi
			fi
		done
	else
		sleep "${sleepTime}"
		secondCommand=$(${executedCommand})
		diff <(echo "${firstCommand}") <(echo "${secondCommand}")
	fi
}

sleepTime=3
change="no"
permanent="no"
while [[ -n ${1-} ]]; do
	case ${1} in
		--sleep )
			shift
			sleepTime=${1};;
		--command )
			shift
			executedCommand=${1}
			help="no";;
		--change )
			change="yes";;
		--permanent )
			permanent="yes";;
		-h | --help )
			Help
			exit 0;;
		* )
			echo -e "${Red}Wrong parameter(s)${NoColor}"
			Help
			exit 1;;
	esac
	shift
done

if [[ -z ${help-} ]]; then
	Help
	exit 1
fi

main
