#!/usr/bin/env bash
set -euo pipefail
if [[ -e /var/lib/pacman/db.lck ]]; then
	echo "Pacman lockfile detected, not doing a check to prevent false positives."
	exit 0
fi
# Compare output of pacman to uname -R, while replacing all dashes for dots. The .0.arch sed is there because .0 kernel releases are named stupidly without the .0 when compared to uname -r
# uname -r && pacman -Q linux(-version)
# 5.2.8-arch1-1-ARCH | linux 5.2.8.arch1-1 # pre-2019-11-11
# 5.3.10-arch1-1     | linux 5.3.10.1-1    # pre-2019-12-02
# 5.4.1-arch1-1      | linux 5.4.1.arch1-1
# 5.3.0-rc4-mainline | linux-mainline 5.3rc4-1
if ! command -v systemd-detect-virt &>/dev/null; then
	echo "Error getting virtualization, no systemd?"
	exit 1
else
	virtualization=$(systemd-detect-virt) || true
fi
if [[ ${virtualization} == "lxc" ]]; then
	echo "System is an LXC container, exiting"
	exit 0
fi
runningKernelFull=$(uname -r)
kernelSuffix="$(echo -${runningKernelFull##*-})"
runningKernel=$(echo ${runningKernelFull} | sed s/${kernelSuffix}// | sed -e s/-[1-9]//g -e s/-/./g -e 's/\.0\./\./g' -e s/arch// |`# mainline workaround` sed s/[.]rc/rc/)
kernelSuffix="$(echo ${kernelSuffix} | sed s/-[1-9]//)"
notify="no"
# If there's a parameter (using --notify, it's a bit hacky to take anything), then send a notification. Called from rebootcheck.service like this.
if [[ -n ${1-} ]]; then
	notify="yes"
fi
if ! installedKernelFull=$(pacman -Q linux${kernelSuffix}); then
	echo "Reboot check script broke. Running kernel ${runningKernel}. Can't get output of pacman -Q linux${kernelSuffix}"
	if [[ ${notify} == "yes" ]]; then
		notify-send "Reboot check script broke." "Running kernel ${runningKernel}.\nCan't get output of 'pacman -Q linux${kernelSuffix}'"
	fi
	exit 1
fi
installedKernel=$(echo "${installedKernelFull##* }" | sed -e s/-[1-9]//g -e s/-/./g -e s/arch//)

if [[ ${runningKernel} != "${installedKernel}" ]]; then
	echo "Running kernel ${runningKernel} does not match installed kernel ${installedKernel}!"
	if [[ ${notify} == "yes" ]]; then
		notify-send "Reboot required!" "Running kernel ${runningKernel} is different from installed kernel ${installedKernel}! \nReboot now to ensure your system behaves correctly.\n
Stop showing the warning for this boot by executing:\n systemctl stop rebootcheck.timer --user"
	fi
else
	echo "Running kernel ${runningKernel} matches installed kernel"
fi
