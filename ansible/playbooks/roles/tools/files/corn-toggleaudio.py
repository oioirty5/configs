#!/usr/bin/env python3
import getpass, subprocess, sys, time
from ruamel.yaml import YAML
from colorama import init

class Colors:
	"Class for coloring output"
	ENDC = '\033[0m'
	BOLD = '\033[1m'
	UNDERLINE = '\033[4m'
	GREY = '\033[90m'
	RED = '\033[91m'
	GREEN = '\033[92m'
	YELLOW = '\033[93m'

def shellExec(args):
	process = subprocess.Popen(args, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
	if debug:
		print(Colors.YELLOW, end = "")
		for i in range(len(args)):
			print(args[i], end = " ")
		print(Colors.ENDC)
	stdout, stderr = process.communicate()
	if stderr != b'':
		print(Colors.RED + str(stderr.rstrip(), "utf-8") + Colors.ENDC)
	return stdout

def getCardList():
	cardListString = shellExec(['pactl', 'list', 'cards', 'short'])
	cardList = {}
	for line in cardListString.decode("utf-8").splitlines():
		card=line.split("\t")
		cardList[card[1]] = card[0]
	return cardList

def getSinkList():
	sinkListString = shellExec(['pactl', 'list', 'sinks', 'short'])
	sinkList = {}
	for line in sinkListString.decode("utf-8").splitlines():
		sink=line.split("\t")
		sinkList[sink[1]] = sink[0]
	return sinkList

def getDeviceList():
	username=getpass.getuser()
	configFile="/home/"+username+"/audioDevices.yaml"
# To get the required values:
# * Run 'pactl list cards short' to get the string
# * Set your device to the correct output and run 'pactl list cards | grep Active' to get output and input
# Example config file:
#PC_Speakers:
#  output: pci-0000_0a_00.4
#  outputMode: analog-stereo
#  index: 1
#TV:
#  output: pci-0000_08_00.1
#  outputMode: hdmi-stereo
#  index: 2
#Valve_Index:
#  virtualSink: True
#  output: pci-0000_08_00.1
#  outputMode: hdmi-stereo-extra2
#  input: usb-Valve_Corporation_Valve_VR_Radio___HMD_Mic_4A8C29B0F1-LYM-01
#  inputMode: mono-fallback
#  index: 3
#Headset:
#  virtualSink: True
#  output: usb-Kingston_HyperX_Virtual_Surround_Sound_00000000-00
#  input: usb-Kingston_HyperX_Virtual_Surround_Sound_00000000-00
#  outputMode: analog-stereo
#  inputMode: iec958-stereo
#  index: 4
	with open(configFile, 'r') as streamConfig:
		yaml = YAML(typ='safe')
		devices = yaml.load(streamConfig)
		#yaml.dump(devices, sys.stdout)
	return devices

# https://wiki.archlinux.org/index.php/PulseAudio/Examples#Mixing_additional_audio_into_the_microphone's_audio
def virtualSink(inputCard, inputIO, outputCard, outputIO, deviceName):
	# Only works on PulseAudio, pipewire-pulse currently does not support the modules we load
	microphone = "alsa_input."+inputCard+"."+inputIO
	speakers = "alsa_output."+outputCard+"."+outputIO # +".monitor" # .monitor only on pipewire-pulse
	micName = deviceName+"_mic_ec"
	spkName = deviceName+"_spk_ec"
	print("Setting up virtual sinks...")
	# Setting up echo cancellation
	shellExec(['pactl', 'load-module', 'module-echo-cancel', 'use_master_format=1', 'aec_method=webrtc',
		'aec_args="analog_gain_control=0\\ digital_gain_control=1\\ experimental_agc=1\\ extended_filter=1"', 'source_master="'+microphone+'"',
		'source_name='+micName, 'source_properties=device.description='+micName+' sink_master="'+speakers+'"', 'sink_name='+spkName, 'sink_properties=device.description='+spkName])
	# Creating virtual output devices
	shellExec(['pactl', 'load-module', 'module-null-sink', 'sink_name=vsink_fx', 'sink_properties=device.description=vsink_fx'])
	shellExec(['pactl', 'load-module', 'module-null-sink', 'sink_name=vsink_fx_mic', 'sink_properties=device.description=vsink_fx_mic'])
	# Creating loopbacks
	shellExec(['pactl', 'load-module', 'module-loopback', 'latency_msec=30', 'adjust_time=3', 'source='+micName, 'sink=vsink_fx_mic'])
	shellExec(['pactl', 'load-module', 'module-loopback', 'latency_msec=30', 'adjust_time=3', 'source=vsink_fx.monitor', 'sink=vsink_fx_mic'])
	shellExec(['pactl', 'load-module', 'module-loopback', 'latency_msec=30', 'adjust_time=3', 'source=vsink_fx.monitor', 'sink='+spkName])
	# Setting default devices
	shellExec(['pactl', 'set-default-source', micName])
	shellExec(['pactl', 'set-default-sink', spkName])

def unloadSinks():
	# In case sinks were loaded for virtual audio, unload them, else PulseAudio screws up
	shellExec(['pactl', 'unload-module', 'module-null-sink'])
	shellExec(['pactl', 'unload-module', 'module-loopback'])
	shellExec(['pactl', 'unload-module', 'module-echo-cancel'])

def setCardMode(cardString, ioString, cardList):
	""" Sets the card to the input/+output mode """
	cardPulseID=""
	for card in cardList:
		if card == cardString:
			cardPulseID=cardList[card]
	cardListString = shellExec(['pactl', 'set-card-profile', cardPulseID, ioString])

def setActive(index, deviceList):
	if debug: print("Setting active index for: " + str(index))
	unloadSinks()
	# Turn off all cards
	for card in cardList:
		cardListString = shellExec(['pactl', 'set-card-profile', cardList[card], 'off'])
	# Turn on all devices matching the index
	for device in deviceList:
		if deviceList[device]['index'] == index:
			deviceName=device
			# Determine if we're setting input and output, or just input or just output
			if 'input' in deviceList[device] and 'output' in deviceList[device]:
				# If the input and output is on the same device, we need to set one mode for one card, as opposed to two modes for two different card
				if deviceList[device]['input'] == deviceList[device]['output']:
					ioString="output:"+deviceList[device]['outputMode']+"+input:"+deviceList[device]['inputMode']
					setCardMode( "alsa_card."+deviceList[device]['output'], ioString, cardList )
				else:
					ioString="input:"+deviceList[device]['inputMode']
					setCardMode( "alsa_card."+deviceList[device]['input'], ioString, cardList )
					ioString="output:"+deviceList[device]['outputMode']
					setCardMode( "alsa_card."+deviceList[device]['output'], ioString, cardList )
			elif 'input' in deviceList[device]:
				ioString="input:"+deviceList[device]['inputMode']
				setCardMode( "alsa_card."+deviceList[device]['input'], ioString, cardList )
			elif 'output' in deviceList[device]:
				ioString="output:"+deviceList[device]['outputMode']
				setCardMode( "alsa_card."+deviceList[device]['output'], ioString, cardList )
			else:
				print("Input nor output defined for device "+deviceList[device])
				exit(1)
			if 'virtualSink' in deviceList[device]:
				if deviceList[device]['virtualSink']:
					virtualSink(deviceList[device]['input'], deviceList[device]['inputMode'], deviceList[device]['output'], deviceList[device]['outputMode'], deviceName)
			print("Switched to ID " + str(index) + " - " + deviceName)

def switchSource(deviceList):
	if not sinkList:
		return(1) # No sinks found
	activeSink=next(iter(sinkList))
	for device in deviceList:
		if 'output' in deviceList[device]: # Ignore entries with input only
			if activeSink == "alsa_output."+deviceList[device]['output']+"."+deviceList[device]['outputMode']:
				currentIndex=deviceList[device]['index']
				if currentIndex == initialDevicesAmount:
					setActive(1, deviceList)
					return(0)
				else:
					setActive(currentIndex+1, deviceList)
					return(0)
	return(1)

def unmuteSink(sinkID):
	shellExec(['pactl', 'set-sink-mute', sinkID, '0'])

debug=True
cardList=getCardList()
sinkList=getSinkList()
deviceList=getDeviceList()
initialDevicesAmount=1
# Get biggest index this way instead of len() since we can have multiple devices as one index
for device in deviceList:
	if deviceList[device]['index'] > initialDevicesAmount: initialDevicesAmount = deviceList[device]['index']
for device in list(deviceList):
	fullCardName="alsa_card."+deviceList[device]['output'] # Kinda hacky but eh
	if fullCardName not in cardList:
		print(Colors.RED + device+" was removed from the list because it was not found to be connected!" + Colors.ENDC)
		del deviceList[device]
if len(deviceList) < 2:
	print(Colors.RED + "There's less than two devices, nothing to toggle between!" + Colors.ENDC)
	exit(1)
if "auto_null" in sinkList:
	print(Colors.RED + "No sources active, defaulting to the first index" + Colors.ENDC)
	setActive(1, deviceList)
else:
	if switchSource(deviceList) == 1:
		print(Colors.RED + "Current source unknown, defaulting to the first index" + Colors.ENDC)
		setActive(1, deviceList)

# Hopeful workaround for a race condition on HDMI devices
time.sleep(0.100)
sinkList=getSinkList()
# Unmute all sinks, for some reason, probably a PA or a KDE update, the sinks start muted after a switch since around KDE 5.20
for sink in sinkList:
	unmuteSink(sink)

if debug:
	print(deviceList)
	print(cardList)
	print(sinkList)
