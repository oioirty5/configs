#!/usr/bin/env bash
set -euo pipefail
Red='\033[0;31m'
Blue='\033[0;94m'
Yellow='\033[0;33m'
NoColor='\033[0m'

boardInfo() {
	if [[ ${virtualization} == "none" ]]; then
		if ! chassis_vendor=$(sudo cat /sys/class/dmi/id/chassis_vendor 2>/dev/null); then
			chassis_vendor="N/A"
		fi
		if [[ "${chassis_vendor}" == "Dell Inc." ]]; then
			# Example URL: https://www.dell.com/support/home/us/en/04/product-support/servicetag/crzghv1/drivers
			echo -e "${Yellow}Firmware update:${NoColor} https://www.dell.com/support/home/us/en/04/product-support/servicetag/${chassis_serial}/drivers"
		elif [[ "${chassis_vendor}" == "LENOVO" ]]; then
			LenovoURL=$(curl -s -L "https://pcsupport.lenovo.com/en/us/api/mse/getproducts?productId=${chassis_serial}" | grep -o -P '(?<=Id":").*(?=","Guid)')
			# Example URL: https://pcsupport.lenovo.com/en/us/products/laptops-and-netbooks/300-series/330-15ikb-type-81de/81de/81de012qin/pf170m4p/downloads
			echo -e "${Yellow}Firmware update:${NoColor} https://pcsupport.lenovo.com/en/us/products/${LenovoURL}/downloads"
		elif [[ "${chassis_vendor}" == "HP" ]]; then
			HPXML=$(curl -s -L "https://ftp.hp.com/pub/pcbios/${board_name}/${board_name}.xml")
			HPLastVersion=$(echo "${HPXML}" | grep -oPm1 "(?<=Ver=\")[^<a-zA-Z\"]+")
			HPLastVersionDate=$(echo "${HPXML}" | grep -oPm1 "(?<=Date=\")[^<a-zA-Z\"]+")
			# Example URL: https://ftp.hp.com/pub/pcbios/1909/1909.xml
			echo -e "${Yellow}Latest UEFI Version:${NoColor} ${HPLastVersion}"
			echo -e "${Yellow}Latest UEFI Release Date:${NoColor} ${HPLastVersionDate}"
		fi
		# Couldn't figure GIGABYTE and ASUS out, their search sucks to automate
	fi
	if ! product_name=$(sudo cat /sys/class/dmi/id/product_name 2>/dev/null); then
		product_name="N/A"
	fi
	if ! board_name=$(sudo cat /sys/class/dmi/id/board_name 2>/dev/null); then
		board_name="N/A"
	fi
	echo -e "${Yellow}Device:${NoColor} ${product_name}"
	if [[ ${board_name} != "N/A" ]]; then
		echo -e "${Yellow}Motherboard:${NoColor} ${board_name}"
		echo -e "${Yellow}  Vendor:${NoColor} $(cat /sys/class/dmi/id/board_vendor)"
	else
		echo -e "${Yellow}Motherboard:${NoColor} ${board_name}"
	fi
	if [[ ${BootMode} == "UEFI" ]]; then
		echo -e "${Yellow}  UEFI Date:${NoColor} $(cat /sys/class/dmi/id/bios_date)"
	else
		echo -e "${Yellow}  UEFI/BIOS Date:${NoColor} $(cat /sys/class/dmi/id/bios_date)"
	fi
	echo -e "${Yellow}    Version:${NoColor} $(cat /sys/class/dmi/id/bios_version)"
	echo -e "${Yellow}    Vendor:${NoColor} $(cat /sys/class/dmi/id/bios_vendor)"
}

pacdiffStatus() {
	if pacnews=$(pacdiff -o); then
		if [[ ${pacnews} != "" ]]; then
			echo -e "${Yellow}Run pacdiff or otherwise take care of the following pacman files:${NoColor}\n${pacnews}"
		fi
	else
		echo -e "${Red}Pacdiff error!${NoColor}"
	fi
}

mirrorListStatus() {
	if [[ -e /tmp/mirrorliststatus ]]; then
		if [ ! -s /tmp/mirrorliststatus ] ; then
			# Delete mirror list status file if empty
			rm /tmp/mirrorliststatus
		else
			# Delete mirror list status file if older than 24 hours
			find /tmp/mirrorliststatus -mmin +1440 -exec rm /tmp/mirrorliststatus \;
		fi
	fi
	if [[ ! -e /tmp/mirrorliststatus ]]; then
		echo -e "${Yellow}Downloading mirror list status...${NoColor}"
		if ! wget -q -O /tmp/mirrorliststatus https://www.archlinux.org/mirrors/status/json/; then
			echo -e "${Red}Couldn't download mirrorlist status${NoColor}"
		fi
	fi
	firstMirror=$(grep -m1 ^Server /etc/pacman.d/mirrorlist)
	firstMirrorFiltered=${firstMirror%%\$*}
	firstMirrorFiltered=${firstMirrorFiltered##*\ }
	if ! mirrorSyncCompletion=$(jq ".urls[] | select(.url==\"${firstMirrorFiltered}\") | .completion_pct" /tmp/mirrorliststatus); then
		echo -e "${Red}Jq failed for ${firstMirrorFiltered}(${firstMirror})!${NoColor}"
	fi
	if [[ ${mirrorSyncCompletion} != "1" ]]; then
		echo -e "${Red}Your mirror ${firstMirrorFiltered} is out of sync - sync completion at ${mirrorSyncCompletion}!${NoColor}"
	fi
}

nvidiaMismatchCheck() {
	if [[ -z ${DISPLAY-} ]]; then
		# Fallback in case the check is ran through tty
		export DISPLAY=':0'
	fi
	if [[ ! -e /usr/bin/nvidia-smi ]]; then
		return
	fi
	if ! smiOutput=$(LANG=C /usr/bin/nvidia-smi); then
		if [[ ${smiOutput} == "Failed to initialize NVML: Driver/library version mismatch" ]]; then
			installedDriverVer=$(nvidia-smi --help | awk 'NR==1{print $6}')
			runningDriverVer=$(nvidia-settings -q NvidiaDriverVersion 2>/dev/null | grep NvidiaDriverVersion | awk 'NR==1{print $4}')
			echo -e "${Red}Installed Nvidia driver ${installedDriverVer} does not match the running driver ${runningDriverVer}!${NoColor}"
			echo -e "${Red}  Reboot ASAP to fix!${NoColor}"
		fi
	else
		echo -e "${Yellow}Nvidia driver:${NoColor} $( echo "${smiOutput}" | awk 'NR==3{ print $3 }')"
	fi
}

Containers() {
	for container in ${lxclist}; do
		spacesToAdd=$((20-${#container}))
		containerSpaced=${container}
		for i in $(seq 1 $((${spacesToAdd}))); do
			containerSpaced="${containerSpaced} "
		done
		containerInfo=$(lxc info "${container}")
		echo -e "${Blue}  ${containerSpaced}${NoColor} $(echo "${containerInfo}" | grep current | \
			awk {'print $3'}) $(lxc config show ${container} | \
			grep memory: | awk {'print $2'})"
		if snapshotList=$(echo "${containerInfo}" | grep stateless); then
			if manualSnapshots=$(echo "${snapshotList}" | grep -v autosnapshot); then
				echo -e "${Red}  Manual snapshots detected!\n ${manualSnapshots}${NoColor}" >&2
			fi
		fi
	done
}

postInstall() {
	cat <<EOF
Plasma taskbar:
* Move up and change the Panel height to 60
* Add Widgets - Event calendar(plasma5-applets-eventcalendar), Memory Usage, Individual Core Usage, Network Speed
* Remove Digital Clock Widget - replaced by Event Calendar
* Event calendar - set first line to HH:mm:ss
* Event calendar - set second line to ddd  yyyy-MM-dd
* Event calendar - Calendar - Style - Show Week Numbers: on
* Event calendar - Calendar - Style - Event Badge: Bottom Bar (Event Color)
* Event calendar - Google Calendar - login to personal acc and enable all calendars
* Event calendar - Timezones - check UTC +0
* Network Speed - Display style: Text Only
* Memory Usage - Display style: Digital Gauge

# Seems not needed anymore on a fresh install
#System Settings -> Personalization -> Applications -> Default applications -> Set default apps
# Trying without
#Display configuration -> Global Scale 125%
Konsole -> Settings -> Configure Konsole... -> General -> Run all Konsole windows in a single process: On
Keyboard -> Advanced -> Configure keyboard options: On
Keyboard -> Advanced -> Position of Compose key: Right alt

KDEConnect -> Configure

Pin:
* Tauon music box
* Mumble
* Jitsi
* virt-manager
* Steam (runtime)
* Deemix
* Dolphin
* Pavucontrol
* Code
* Chromium
* Firefox
* KeepassXC
* qbittorrent
* Telegram
EOF
}

# Get root perms at the beggining instead of the end
if ! chassis_serial=$(sudo cat /sys/class/dmi/id/chassis_serial 2>/dev/null); then
	chassis_serial="N/A"
fi
if [[ -d /sys/firmware/efi ]]; then BootMode="UEFI"; else BootMode="BIOS"; fi
source /etc/os-release
if [[ ${NAME} != "Arch Linux" ]] && [[ ${NAME} != "Arch Linux ARM" ]]; then
	echo -e "${Red}UNSUPPORTED OS${NoColor}"
fi
if ! command -v systemd-detect-virt &>/dev/null; then
	echo -e "${Red}Error getting virtualization, no systemd?${NoColor}"
else
	virtualization=$(systemd-detect-virt) || true
fi
#echo -e "${Yellow}LAN IPv4 addresses:${NoColor} $(hostname -i)"
echo -e "${Yellow}Hostname:${NoColor} $(hostname)"
echo -e "${Yellow}Kernel:${NoColor} $(uname -r)"
if [[ ${virtualization} != "lxc" ]] && [[ ${NAME} == "Arch Linux" ]]; then
	if rebootcheck=$(corn-rebootcheck); then
		if echo "${rebootcheck}" | grep 'does not match' > /dev/null; then
			echo -e "${Red}${rebootcheck}${NoColor}"
			echo -e "${Red}  Reboot at your convenience to fix!${NoColor}"
		fi
	else
		echo -e "${Red}corn-rebootcheck broke${NoColor}"
	fi
fi
echo -e "${Yellow}Uptime:${NoColor} $(uptime -p)"
echo -e "${Yellow}Virtualization:${NoColor} ${virtualization}"
if ! timedatectl status | grep "synchronized: yes" >/dev/null; then
	echo -e "${Red}NTP: No${NoColor}"
fi
echo -e "${Yellow}Boot mode:${NoColor} ${BootMode}"
if [[ ${virtualization} != "lxc" ]]; then
	if [[ -e /boot/EFI/systemd/systemd-bootx64.efi ]]; then
		currentSystemd=$(systemctl --version | head -n1 | awk '{print substr($3,2,length($3) -2)}')
		installedSystemdBoot=$(strings /boot/EFI/systemd/systemd-bootx64.efi | grep LoaderInfo | awk '{print $4}')
		if [[ ${currentSystemd} != "${installedSystemdBoot}" ]]; then
			echo -e "${Red}systemd-boot version ${installedSystemdBoot} is different from installed systemd ${currentSystemd}${NoColor}"
			echo -e "${Red}  Run 'sudo bootctl update' to fix!${NoColor}"
		fi
	fi
fi
if [[ ${NAME} == "Arch Linux" ]]; then
	nvidiaMismatchCheck
	boardInfo
	# This isn't a 100% detection for a broken mirror plus it is currently made in an annoying fashion which blocks the terminal
	#mirrorListStatus
fi

if lxclist=$(lxc list --fast 2>/dev/null | grep "RUNNING" | awk {'print $2'}); then
	echo -e "${Yellow}Containers:${NoColor}"
	Containers
fi

pacdiffStatus
if [[ -n ${1-} ]]; then # Hack, but eh
	postInstall
fi
