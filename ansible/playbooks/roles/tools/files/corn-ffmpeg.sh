#!/usr/bin/env bash
set -euo pipefail
Red='\033[0;31m'
Yellow='\033[0;33m'
NoColor='\033[0m'

Help() {
	echo -e "${Yellow}"
cat << EOF
Options:
--file                              Video file name
--timefrom                          Time to cut from ie 00:01:00
--timeto                            Time to cut to   ie 00:01:30
-h | --help                         Show this help...
EOF
	echo -e "${NoColor}"
}

while [[ -n ${1-} ]]; do
	case ${1} in
		--file )
			shift
			fullFileName=${1}
			fileName="${fullFileName%%.*}"
			fileExtension=".${fullFileName##*.}";;
		--timefrom )
			shift
			timeFrom=${1};;
		--timeto )
			shift
			timeTo=${1};;
		-h | --help )
			Help
			exit 0;;
		* )
			Help
			exit 1;;
	esac
	shift
done

if ffmpeg -i "${fileName}${fileExtension}" -ss ${timeFrom} -to ${timeTo} -c copy "${fileName}-cut${fileExtension}"; then
	echo -e "${Yellow}Done!${NoColor}"
else
	Help
	echo -e "${Red}Something broke!${NoColor}"
fi

# The first -ss seeks fast to (approximately) 8min0sec, and then the second -ss seeks accurately to 9min0sec, and the -t 00:01:00 takes out a 1min0sec clip.
# ffmpeg -ss 00:08:00 -i Video.mp4 -ss 00:01:00 -t 00:01:00 -c copy VideoClip.mp4

#rm -f /tmp/mylist.txt && for f in ~/*-cut*; do echo "file '$f'" >> /tmp/mylist.txt; done
#ffmpeg -f concat -safe 0 -i /tmp/mylist.txt -c copy ~/stitch.mkv

# Create GIF
# from a full file
#ffmpeg -ss 30 -t 3 -i input.mp4 -vf "fps=10,scale=320:-1:flags=lanczos,split[s0][s1];[s0]palettegen[p];[s1][p]paletteuse" -loop 0 output.gif
# from an already cut file
#ffmpeg -i Remembering.mkv -vf "fps=10,scale=320:-1:flags=lanczos,split[s0][s1];[s0]palettegen[p];[s1][p]paletteuse" -loop 0 Remembering.gif
# with subtitles
#ffmpeg -i Remembering.mkv -vf "fps=10,scale=320:-1:flags=lanczos,subtitles=Remembering.mkv:force_style='Fontsize=24',split[s0][s1];[s0]palettegen[p];[s1][p]paletteuse" -loop 0 Remembering.gif
# force font name to avoid rendering issues
#ffmpeg -i Remembering.mkv -vf "fps=10,scale=320:-1:flags=lanczos,subtitles=Remembering.mkv:force_style='Fontsize=60,FontName=DejaVu Serif',split[s0][s1];[s0]palettegen[p];[s1][p]paletteuse" -loop 0 Remembering.gif
# extract subtitles (get map from ffmpeg -i input.mkv)
#ffmpeg -i input.mkv -map 0:7 -c copy output_subtitle.ass

#:force_style='FontName=DejaVu Serif'
