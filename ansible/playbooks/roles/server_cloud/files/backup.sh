#!/usr/bin/env bash
set -euo pipefail

todayDate=$(date --iso-8601)
archiveName="/root/$(hostname)-${todayDate}.tzst"

mysqldump --defaults-file=/root/.my.cnf nextcloud > /root/nextcloud_db_${todayDate}.sql

tar -acvf ${archiveName} \
	/root/nextcloud_db_${todayDate}.sql \
	/etc/webapps/nextcloud/config/config.php

# Upload to my Nextcloud instance
cloudsend --file ${archiveName}

# Cleanup
rm -f ${archiveName} \
	/root/nextcloud_db_${todayDate}.sql \
