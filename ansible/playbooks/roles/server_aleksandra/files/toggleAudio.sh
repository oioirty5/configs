#!/bin/bash
set -euo pipefail
gpu="pci-0000_01_00.1"
headphones="C-Media_Electronics"
pcSpeakers="pci-0000_00_1b.0"
gpuID=$(pactl list cards short | grep "alsa_card.${gpu}" | awk '{ print $1 }')
headphonesID=$(pactl list cards short | grep "${headphones}" | awk '{ print $1 }')
pcSpeakersID=$(pactl list cards short | grep "alsa_card.pci-${pcSpeakers}" | awk '{ print $1 }')
pactl set-card-profile ${gpuID} off
# If the current sink contains C-Media_Electronics then it's my headphones in my 1.18 USD sound card, so switch it to speakers. If not do the opposite
if pactl list short sinks | grep "${headphones}" > /dev/null; then
	# Note - my motherboard is buggy and the audio takes some random fiddling with to play correctly, and it can bug out the moment you switch outputs
	pactl set-card-profile ${pcSpeakersID} output:analog-stereo
	pactl set-card-profile ${headphonesID} off
	echo "Toggled from Headset to Speakers."
else
	pactl set-card-profile ${headphonesID} output:analog-stereo
	# Note - because my sound card on mobo is buggy, Mumble needs set RNNoise
	pactl set-card-profile ${pcSpeakersID} input:analog-stereo
	echo "Toggled from Speakers to Headset."
fi
