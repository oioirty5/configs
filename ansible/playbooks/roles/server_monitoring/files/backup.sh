#!/usr/bin/env bash
set -euo pipefail

todayDate=$(date --iso-8601)
archiveName="/root/$(hostname)-${todayDate}.tzst"

# Cleanup in case previous run failed
rm -f /tmp/director_${todayDate}.sql /tmp/icinga_${todayDate}.sql /tmp/icingaweb2_${todayDate}.sql

# https://icinga.com/docs/icinga-2/latest/doc/02-installation/#backup
sudo -u postgres pg_dump director > /tmp/director_${todayDate}.sql
sudo -u postgres pg_dump icinga > /tmp/icinga_${todayDate}.sql
sudo -u postgres pg_dump icingaweb2 > /tmp/icingaweb2_${todayDate}.sql

tar -acvf ${archiveName} \
	/etc/icinga2 \
	/etc/icingaweb2 \
	/var/lib/icinga2 \
	/tmp/director_${todayDate}.sql \
	/tmp/icinga_${todayDate}.sql \
	/tmp/icingaweb2_${todayDate}.sql

# Upload to my Nextcloud instance
cloudsend --file ${archiveName}

# Cleanup
rm -rf ${archiveName} \
	/tmp/director_${todayDate}.sql /tmp/icinga_${todayDate}.sql /tmp/icingaweb2_${todayDate}.sql
