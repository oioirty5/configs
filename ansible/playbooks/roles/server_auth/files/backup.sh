#!/usr/bin/env bash
set -euo pipefail

todayDate=$(date --iso-8601)
archiveName="/root/$(hostname)-${todayDate}.tzst"

# Cleanup in case previous run failed
rm -f /root/keycloak.json
pkill -9 -G0 -e java || true

# https://www.keycloak.org/docs/latest/server_admin/index.html#_export_import

# Start an extra copy of Keycloak in the background, offsetting the ports by 100 to not conflict with the live one, and backup to .json
/opt/keycloak/bin/standalone.sh \
	-Dkeycloak.migration.action=export \
	-Dkeycloak.migration.provider=singleFile \
	-Dkeycloak.migration.file=/root/keycloak.json \
	-Djboss.socket.binding.port-offset=100 &

# 60 should be more than plenty of time for KC to kick into high gear, should be replace by some curl check maybe
# Only kill the process owner by root, not the live one owned by keycloak
sleep 60 && pkill -9 -G0 -e java

tar -acvf ${archiveName} \
	/root/keycloak.json

# Upload to my Nextcloud instance
cloudsend --file ${archiveName}

# Cleanup
rm -f ${archiveName} \
	/root/keycloak.json
