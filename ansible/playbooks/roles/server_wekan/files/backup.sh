#!/usr/bin/env bash
set -euo pipefail

todayDate=$(date --iso-8601)
archiveName="/root/$(hostname)-${todayDate}.tzst"

rm -rf /root/mongodump # Cleanup in case previous run failed
# https://github.com/wekan/wekan/wiki/Backup#backup-wekan-snap-to-directory-dump
snap stop wekan.wekan
export LD_LIBRARY_PATH=${LD_LIBRARY_PATH-}:/var/lib/snapd/snap/wekan/current/lib/x86_64-linux-gnu
export PATH="${PATH}:/var/lib/snapd/snap/wekan/current/bin"
mongodump --port 27019 -o /root/mongodump
snap get wekan > /root/snap-settings.sh
snap start wekan.wekan

tar -acvf ${archiveName} \
	/root/mongodump \
	/root/snap-settings.sh

# Upload to my Nextcloud instance
cloudsend --file ${archiveName}

# Cleanup
rm -rf ${archiveName} \
	/root/mongodump
