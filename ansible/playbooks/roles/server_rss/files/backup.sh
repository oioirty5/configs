#!/usr/bin/env bash
set -euo pipefail

todayDate=$(date --iso-8601)
archiveName="/root/$(hostname)-${todayDate}.tzst"

rm -f /root/freshrss.sqlite # Cleanup in case previous run failed
# https://freshrss.github.io/FreshRSS/en/admins/05_Backup.html
/usr/share/webapps/freshrss/cli/export-sqlite-for-user.php --user admin --filename /root/freshrss.sqlite

tar -acvf ${archiveName} \
	/root/freshrss.sqlite

# Upload to my Nextcloud instance
cloudsend --file ${archiveName}

# Cleanup
rm -f ${archiveName} \
	/root/freshrss.sqlite
