worker_processes 1;
events {
	worker_connections 1024;
}

http {
	ssl_dhparam /etc/ssl/certs/dhparam.pem;
	ssl_certificate /etc/letsencrypt/live/proxy.rys.pw/fullchain.pem;
	ssl_certificate_key /etc/letsencrypt/live/proxy.rys.pw/privkey.pem;
	ssl_protocols TLSv1.2 TLSv1.3;
	ssl_prefer_server_ciphers on;
	ssl_ciphers "EECDH+AESGCM:EDH+AESGCM:AES256+EECDH:AES256+EDH";
	ssl_ecdh_curve secp384r1;
	ssl_session_cache shared:SSL:10m;
	ssl_session_tickets off;
	ssl_stapling on;
	ssl_stapling_verify on;
	add_header Strict-Transport-Security "max-age=63072000; includeSubdomains; preload";

	client_max_body_size 20M;

	include mime.types;
	default_type application/octet-stream;
	sendfile on;
	keepalive_timeout 65;
	types_hash_max_size 4096;

	server {
		server_name wekan.rys.pw;
		listen 443 ssl http2;
		listen [::]:443 ssl http2;
		location / {
			proxy_set_header Host $host;
			proxy_set_header X-Real-IP $remote_addr;
			proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
			proxy_set_header X-Forwarded-Proto $scheme;
			proxy_pass http://192.168.1.23:8080;
			proxy_read_timeout 90;
		}
	}
	server {
		server_name monitoring.rys.pw;
		listen 443 ssl http2;
		listen [::]:443 ssl http2;
		location / {
			proxy_set_header Host $host;
			proxy_set_header X-Real-IP $remote_addr;
			proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
			proxy_set_header X-Forwarded-Proto $scheme;
			proxy_pass http://192.168.1.24;
			proxy_read_timeout 90;
		}
	}
	server {
		server_name rss.rys.pw;
		listen 443 ssl http2;
		listen [::]:443 ssl http2;
		location / {
			proxy_set_header Host $host;
			proxy_set_header X-Real-IP $remote_addr;
			proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
			proxy_set_header X-Forwarded-Proto $scheme;
			proxy_pass http://192.168.1.21;
			proxy_read_timeout 90;
		}
	}
	server {
		server_name cloud.rys.pw;
		listen 443 ssl http2;
		listen [::]:443 ssl http2;
		# https://central.owncloud.org/t/request-entity-too-large-nginx-server/1795/4
		proxy_buffering off;
		proxy_request_buffering off;
		client_max_body_size 10G;
		location / {
			proxy_set_header Host $host;
			proxy_set_header X-Real-IP $remote_addr;
			proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
			proxy_set_header X-Forwarded-Proto $scheme;
			proxy_pass http://192.168.1.22;
			proxy_read_timeout 90;
		}
		location ^~ /push/ {
			proxy_pass http://192.168.1.22:7867/;
			proxy_http_version 1.1;
			proxy_set_header Upgrade $http_upgrade;
			proxy_set_header Connection "Upgrade";
			proxy_set_header Host $host;
			proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
		}
	}
	server {
		server_name radio.rys.pw;
		listen 443 ssl http2;
		listen [::]:443 ssl http2;
		root /var/www/html/radio;
		location /stream.ogg {
			proxy_pass http://192.168.1.10:7590;
		}
		location /radio/getpic {
			proxy_pass http://192.168.1.10:7590;
		}
		location /radio/update_radio {
			proxy_pass http://192.168.1.10:7590;
		}
	}
	server {
		server_name books.rys.pw;
		listen 443 ssl http2;
		listen [::]:443 ssl http2;
		location / {
			proxy_pass http://192.168.1.38:4180;
		}
	}
	server {
		server_name auth.rys.pw;
		listen 443 ssl http2;
		listen [::]:443 ssl http2;
		location / {
			proxy_pass http://192.168.1.28:8080;
		}
	}
	server {
		server_name jellyfin.rys.pw;
		listen 443 ssl http2;
		listen [::]:443 ssl http2;

		set $jellyfin 192.168.1.39;
		add_header X-Frame-Options "SAMEORIGIN";
		add_header X-XSS-Protection "1; mode=block";
		add_header X-Content-Type-Options "nosniff";

		# Content Security Policy
		# See: https://developer.mozilla.org/en-US/docs/Web/HTTP/CSP
		# Enforces https content and restricts JS/CSS to origin
		# External Javascript (such as cast_sender.js for Chromecast) must be whitelisted.
		#add_header Content-Security-Policy "default-src https: data: blob:; style-src 'self' 'unsafe-inline'; script-src 'self' 'unsafe-inline' https://www.gstatic.com/cv/js/sender/v1/cast_sender.js; worker-src 'self' blob:; connect-src 'self'; object-src 'none'; frame-ancestors 'self'";

		location / {
			# Proxy main Jellyfin traffic
			proxy_pass http://$jellyfin:8096;
			proxy_set_header Host $host;
			proxy_set_header X-Real-IP $remote_addr;
			proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
			proxy_set_header X-Forwarded-Proto $scheme;
			proxy_set_header X-Forwarded-Protocol $scheme;
			proxy_set_header X-Forwarded-Host $http_host;

			# Disable buffering when the nginx proxy gets very resource heavy upon streaming
			proxy_buffering off;
		}

		# location block for /web - This is purely for aesthetics so /web/#!/ works instead of having to go to /web/index.html/#!/
		location ~ ^/web/$ {
			# Proxy main Jellyfin traffic
			proxy_pass http://$jellyfin:8096/web/index.html/;
			proxy_set_header Host $host;
			proxy_set_header X-Real-IP $remote_addr;
			proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
			proxy_set_header X-Forwarded-Proto $scheme;
			proxy_set_header X-Forwarded-Protocol $scheme;
			proxy_set_header X-Forwarded-Host $http_host;
		}

		location /socket {
			# Proxy Jellyfin Websockets traffic
			proxy_pass http://$jellyfin:8096;
			proxy_http_version 1.1;
			proxy_set_header Upgrade $http_upgrade;
			proxy_set_header Connection "upgrade";
			proxy_set_header Host $host;
			proxy_set_header X-Real-IP $remote_addr;
			proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
			proxy_set_header X-Forwarded-Proto $scheme;
			proxy_set_header X-Forwarded-Protocol $scheme;
			proxy_set_header X-Forwarded-Host $http_host;
	}
}

	server {
		listen 80;
		listen [::]:80;
		server_name proxy.rys.pw auth.rys.pw books.rys.pw cloud.rys.pw jellyfin.rys.pw monitoring.rys.pw radio.rys.pw rss.rys.pw wekan.rys.pw;
		return 301 https://$host$request_uri;
	}
}
