#!/usr/bin/env bash
set -euo pipefail

host="ansible.rys.pw"
port="10250"

echo "Syncing playbooks, unit files and hosts"
rsync -avzz -e "ssh -p $port" hosts root@$host:/etc/ansible/
rsync -avzz -e "ssh -p $port" --delete playbooks root@$host:/root/
rsync -avzz -e "ssh -p $port" ./unit_files/* root@$host:/etc/systemd/system/

echo "Reloading systemd and enabling/starting unit files"
ssh -p $port root@$host "systemctl daemon-reload"
ssh -p $port root@$host "systemctl enable --now update.timer && systemctl enable --now updateServers.timer"

echo "All OK!"
